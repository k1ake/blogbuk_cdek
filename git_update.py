# https://azzamsa.com/n/gitpython-intro/

import shutil, os, git

CONFIG_NAME = "config.py"
REPO_NAME = "blogbuk_cdek"
REPO_PATH = os.environ["HOME"] + "/mygit/" + REPO_NAME + "/"


def config_no_credentials(config_name: str):
    conf_arr = list()

    with open(CONFIG_NAME, "r") as f:
        for line in f:
            if " = \"" in line:
                line = line.split(" ")
                line[-1] = "\"\""
                line = " ".join(line) + "\n"
            conf_arr.append(line)

    print(REPO_PATH + CONFIG_NAME)
    with open(REPO_PATH + CONFIG_NAME, "w") as f:
        f.writelines(conf_arr)


def move_files(file_list: list, dest: str):
    try:
        os.mkdir(dest)
    except FileExistsError:
        pass

    for i in file_list:
        shutil.copyfile(i, dest + i.split("/")[-1])


def git_update(repo_name: str):
    commit_msg = input("Commit message: ")

    repo = git.Repo(REPO_PATH)
    repo.git.add(REPO_PATH + "*")
    repo.git.commit(m=commit_msg)

    repo.git.push(f"git@gitlab.com:k1ake/{REPO_NAME}.git")
    print("Done")


def main():
    files_to_move = [
        "main.py",
        CONFIG_NAME,
        "base.json",
        "makepdf.json",
        "important/README.md",
        "important/git_update.py"
    ]

    move_files(files_to_move, REPO_PATH)
    config_no_credentials(CONFIG_NAME)
    git_update(REPO_NAME)


if __name__ == "__main__":
    main()
