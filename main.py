from dataclasses import dataclass, field, asdict
from time import sleep, time, localtime
from shutil import copyfile
import requests, json, re, logging, os
import config


class CI(Exception):
    """
    Ошибка для пропуска книг
    """

    msg = "Пропуск книги..."

    def __str__(self) -> None:
        return self.msg

    def __init__(self, msg: str, *args) -> None:
        self.msg = msg
        super().__init__(args)


class WireCRM_API:
    """
    Класс для работы с книгами в WireCRM
    """

    def __init__(self, key: str) -> None:
        """
        Инициализация экземпляра класса

        Args:
            key (str): API ключ от WireCRM
        """
        self.head = {"X-API-KEY": key}
        self.all_orders = self.get_orders()

    def get_orders(self) -> dict:
        """
        Получает последние заказы из CRM и пишет их в файл crm.json

        Returns:
            dict: Словарь состоящий из последних заказов из CRM
        """
        lim = 100
        url = f"https://wirecrm.com/api/v1/deals?limit={lim}"
        r = requests.get(url, headers=self.head).json()
        with open("tmp/crm.json", "w", encoding="utf-8") as f:
            json.dump(r["data"], f)
        logging.info("Последние " + str(lim) + " заказов сохранены в файл 'crm.json'")
        return r["data"]

    def get_book_info(self, id: int) -> dict:
        """
        Из всех заказов выбирает исключительно информацию по интересующей нас книге

        Args:
            id (int): Номер книги у блогбук

        Returns:
            dict: Вся информация по книге
        """
        for order in self.all_orders["rows"]:
            if re.findall(r"\d{4,}", order["name"])[0] == str(id):
                order["book"] = str(id)
                return order
        else:
            print("Not found")
            raise CI("Книга с таким номером не найдена")


class CDEK_API:
    """
    Класс для работы с API сдэк
    """

    delivery_types = {
        "Самовывоз": 0,
        "Посылка склад-склад.": 136,
        "Посылка склад-дверь.": 137,
        "Экономичная посылка склад-дверь.": 233,
        "Экономичная посылка склад-склад.": 234,
        "Экспресс склад-дверь.": 294,
        "Посылка склад-постамат.": 368,
        "Экономичная посылка склад-постамат.": 378,
    }

    def __init__(self, cdek_urls: dict, id: str, secret: str) -> None:
        """Инициализация класса

        Args:
            cdek_urls (dict): Содержит ссылки для работы с API
            id (str): Идентификатор для доступа к API
            secret (str): Секретный ключ для доступа к API
        """
        self.urls = cdek_urls
        params = {
            "grant_type": "client_credentials",
            "client_id": id,
            "client_secret": secret, }

        r = requests.post(self.urls["HEAD"], params=params).json()

        token = r["access_token"]

        self.head = {"Authorization": "Bearer {}".format(token),
                     "Content-Type": "application/json", }

        logging.info("Создан заголовок для СДЭКа")

    def get_delievery_points(self) -> dict:
        r = requests.get(self.urls["DELI"], headers=self.head).json()
        with open("tmp/deli_points.json", "w") as f:
            json.dump(r, f)
        return r

    def send_to_cdek(self, data: dict):
        """
        Отправляет созданный заказ в СДЭК, возвращает ответ r от сервера и uuid заказа для работы с заказом в дальнейшем
        Требует:
            data - Собранный в соответствии с требованиями СДЭК словарь со всей необходимой информацией по заказу
            head - Словарь-заголовок для СДЭК, создаётся функцией head_create()
        Возвращает:
            r - Полный ответ от сервера СДЭК
            uuid - uuid заказа в системе
        """
        r = requests.post(self.urls["SEND"], headers=self.head, json=data).json()
        try:
            uuid = r["entity"]["uuid"]
        except KeyError:
            raise CI("Нет поля uuid для отправки, что-то пошло не так ранее")

        logging.info("Ответ от СДЭКа на отправку json'a:")
        logging.debug(f"\t{repr(r)}")

        # Записываем номер заказа в текстовый документ и его uuid, для возможной работы с ним в дальнейшем
        with open("Обработанные заказы.txt", "a+", encoding="utf-8") as f:
            f.write(f"{data['number']}-->{uuid}\n")
            logging.info("Заказ " + data['number'] + " сохранён")
        sleep(5)
        return r, uuid

    def get_info(self, order_uuid: str):
        """
        Выкачивает всю информацию из СДЭК по нашему заказу
        Требует:
            order_uuid - uuid заказа, возвращаемый функцией send_to_cdek()
            head - Словарь-заголовок для СДЭК, получаемый функцией head_create()
        Возвращает:
            r - Полный ответ от сервера
        Записывает всю информацию от СДЭК по заказу в файл ./tmp/info.json
        """
        url = self.urls["INFO"] + order_uuid
        r = requests.get(url, headers=self.head).json()
        with open("tmp/info.json", "w", encoding="utf-8") as f:
            json.dump(r, f)
        return r

    def make_receipt(self, order_uuid: str):
        """
        Отправляет заявку в СДЭК на генерацию квитанции, возвращает ответ от сервера r и uuid квитанции
        Требует:
            order_uuid - uuid заказа, возвращаемый функцией send_to_cdek()
            head - Словарь-заголовок для СДЭК, получаемый функцией head_create()
        Возвращает:
            r - Полный ответ от сервера
            uuid - uuid квитанции в системе СДЭК
        """
        with open("makepdf.json", "r") as f:
            data = json.load(f)
        data["orders"][0]["order_uuid"] = order_uuid

        for _ in range(3):
            sleep(1)
            r = requests.post(self.urls["MPDF"], headers=self.head, json=data).json()

            uuid = r["entity"]["uuid"]
            if "errors" not in r["requests"][0].keys():
                break
        else:
            logging.error("\tmake_receipt: %s", repr(r))
            raise CI("Ошибка в создании квитанции со стороны СДЕК ")
        return r, uuid

    def get_receipt(self, order_uuid: str, book_number: int):
        """
        Получает квитанцию к заказу от СДЭК, квитанцию сохраняет в папку ./pdf/, возвращает ответ от сервера r
        Требует:
            order_uuid - uuid заказа, возвращаемый функцией send_to_cdek()
            head - Словарь-заголовок для СДЭК, получаемый функцией head_create()
        Возвращает:
            r - Полный ответ от сервера
        """
        url = self.urls["GPDF"] + order_uuid
        r = requests.get(url, headers=self.head).json()

        logging.debug("\tget_receipt: %s", repr(r))

        # Проверка на статус заказа в системе, если сохранён с ошибками, то ничего не делаем
        receipt_status = r["requests"][0]["state"].upper()

        match receipt_status:
            case "INVALID":
                logging.error("Не получается оформить ПДФ")
                action = "break"
            case "ACCEPTED":
                logging.info("ПДФ принят в обработку, ждём..")
                action = "wait"
            case "WAITING":
                logging.info("ПДФ всё ещё в обработке, ждём..")
                action = "wait"
            case "SUCCESSFUL":
                logging.info("ПДФ готов, скачиваем")
                action = "download"

        # Выполняем действия в зависимости от статуса заказа
        # Проблема на стороне СДЭК, прерываемся
        if action == "break":
            logging.error("ПДФ не был скачан, смотри ответ от сервера выше")
            print("Получить ПДФ не удалось, смотри логи")
            raise CI("Не удалось получить пдф, отказ со стороны СДЭК")

        # Ожидание пока появится квитанция на их стороне
        elif action == "wait":
            logging.info("Ждём ПДФ от СДЭК")
            while action:
                sleep(5)
                r = requests.get(url, headers=self.head).json()
                if r["requests"][0]["state"].upper() == "SUCCESSFUL":
                    action = "download"
                    break
                else: print("Это может занять некоторое время.."); logging.warning("Всё ещё ждём ПДФ..")

        # Скачивание ПДФ файла, ожидание не трогать т.к. у СДЭК тормозят сервера
        if action == "download":
            logging.info("Пытаемся скачать ПДФ")
            counter = 0
            while True:
                try:
                    pdf_url = r["entity"]["url"]
                    break
                except:
                    print("Пытаемся получить ссылку от СДЭК на квитанцию")
                    logging.warning("Пытаемся получить ссылку от СДЭКа на квитанцию")
                    sleep(5)
                    r = requests.get(url, headers=self.head).json()
                    counter += 1
                if counter > 5:
                    logging.error("Не получается получить ссылку на ПДФ")
                    logging.info(repr(r))

            # Записываем пдфник в папку .pdf/
            pdf = requests.get(url=pdf_url, headers=self.head)
            logging.info("Пишем ПДФник для текущего заказа")
            with open("pdf/" + str(book_number) + ".pdf", "wb") as f:
                f.write(pdf.content)
                logging.info("ПДФник готов!")

            # Копируем json по заказу в папку .json/ если необходимо будет проверить данные по заказу
            copyfile("tmp/info.json", "json/" + str(book_number) + ".json")
            with open("json/" + str(book_number) + ".json", "r") as f:
                tmp = json.load(f)
            try:
                cdek_number = tmp["entity"]["cdek_number"]
                print(f"===============\nКнига:{book_number}\nСДЭК {cdek_number}\n===============")
            except:
                logging.error("Не получается получить номер заказа, вероятно используются тестовые данные")
                print("Не получается получить номер заказа в СДЭК")

        return r


@dataclass()
class Book:
    """
    Создаёт экземпляр книги, получая всю информацию по ней из CRMовского жсона
    """
    _crm_info: dict = field(default_factory=dict, repr=False)
    number: int = field(init=False)
    customer: str = field(init=False)
    phone: str = field(init=False)
    e_mail: str = field(init=False)
    cost: int = field(init=False)
    weight: int = field(init=False)
    tariff_code: int = field(init=False)
    delivery_point: str = field(init=False)
    delivery_recipient_cost: int = field(init=False)

    def __post_init__(self) -> None:
        custom_fields = self._crm_info["custom_fields"]
        try:
            self.number = self._crm_info["book"]
            self.customer = custom_fields[1]["value"]
            self.phone = custom_fields[2]["value"]
            self.e_mail = custom_fields[3]["value"]
            self.cost = int(self._crm_info["description"].split("Сумма: ")[1].split(" ")[0])
            self.weight = Scriptlets.calc_value(self.cost)
            self.delivery_point, \
                self.tariff_code, \
                self. delivery_recipient_cost = Scriptlets.delivery_parse(custom_fields[4]["value"])
        except IndexError:
            raise CI("Ошибка индексов, вероятно, книга не готова")


def write_json(data_dict: dict, test=False) -> dict:

    with open("base.json", "r", encoding="utf-8") as f: order = json.load(f)

    # Куда доставляем:

    tariff_name = [i for i in CDEK_API.delivery_types if CDEK_API.delivery_types[i] == data_dict["tariff_code"]]

    if not tariff_name[0].endswith("дверь."):
        order.pop("to_location")
        order["delivery_point"] = data_dict["delivery_point"]
    else:
        order["to_location"]["address"] = data_dict["delivery_point"]

    # Кому доставляем:
    order["recipient"]["name"] = data_dict["customer"]
    order["recipient"]["phones"][0]["number"] = data_dict["phone"]

    # За сколько доставляем:
    order["tariff_code"] = data_dict["tariff_code"]
    order["delivery_recipient_cost"]["value"] = data_dict["delivery_recipient_cost"]

    # Что доставляем:
    order["number"] = "ББ-" + data_dict["number"]
    order["packages"][0]["number"] = "ББ-" + data_dict["number"]
    order["packages"][0]["items"][0]["cost"] = data_dict["cost"]
    order["packages"][0]["items"][0]["weight"] = data_dict["weight"]
    order["packages"][0]["weight"] = data_dict["weight"]

    if not config.c_type:
        order["shipment_point"] = "MSK142"
        order["number"] += "+4"
        if "delivery_point" in order.keys():
            order["delivery_point"] = "CHEL4"

    with open("tmp/cdek.json", "w", encoding="utf-8") as f:
        json.dump(order, f)
    logging.info("Весь json записан в 'cdek.json'")
    return order


class Scriptlets:
    def current_datetime():
        """
        Возвращает отформатированное время и дату для логов
        Возвращает массив dt[date time]
        """
        datetime = localtime(time())
        dtime = f"{datetime.tm_hour:02}:{datetime.tm_min:02}:{datetime.tm_sec:02}"
        date = f"{datetime.tm_mday:02}.{datetime.tm_mon:02}.{datetime.tm_year-2000}"
        return [date, dtime]

    def dir_check():
        """
        Проверка на существование папок tmp и pdf
        tmp - папка для хранения логов, и временных файлов json
        pdf - папка для хранения пдфов
        json - папка для хранения json'ов с заказами
        """
        if not os.path.isdir("tmp"):
            os.mkdir("tmp")

        if not os.path.isdir("pdf"):
            os.mkdir("pdf")

        if not os.path.isdir("json"):
            os.mkdir("json")

    def operating_mode(status: int = 0):
        """
        Определяет режим работы скрипта:
        Требует:
            status:
                0 - Ручной режим работы
                1 - Автоматический режим
        """
        print("Для запуска автоматического режима нажми: Enter/Y/y/1")
        print("Для ввода книг вручную что-либо кроме того что выше")
        match input():
            case "y" | "Y" | "1" | "": status = 1
            case _: status = 0
        if not status:
            logging.info("Ручной режим")
            books = [int(i) for i in input("Введите номера книг через пробел: ").split(" ")]
            return books

        if status == 1:
            logging.info("Автоматический режим")
            books, together, sent = Scriptlets.scraper(config.ap5Login, config.ap5Password)
            print(f"Задействованы в скрипте будут: {' '.join(books)}")
            if sent: print(f"Отмечены как выданные: {' '.join(sent)}")
            if together: print(f"Вместе идут: {' '.join(together)}")

        return books

    def scraper(login: str, password: str):
        """
        Получение книг из базы данных типографии
        Требует:
            login, password - логин и пароль для доступа к сайту
        Возвращает:
            orders.keys() - Номера книг, ожидающих обработки скриптом
            together - Номера книг, которые идут совместно, для обработки вручную
            sent - Номера книг, отмеченные как переданные
        """
        url = "https://" + login + ":" + password + "@ap5.ponomeram.ru/json.php?json="
        r = requests.get(url)

        data = json.loads(r.text)["orders"]
        orders = dict()
        for _, book in enumerate(data):
            if book["delivery"] != "SDEK": continue
            orders[book["orderNumber"]] = [book["currentStatus"]["status"], book["comment"]]

        together = list()
        sent = [i for i, v in orders.items() if "SENT" in v]

        pattern = r"\d{4,}"
        together = [f"{i}+{'+'.join(re.findall(pattern, v[1]))}" for i, v in orders.items() if "вместе" in v[1]]
        remove = list()
        for i in together:
            [remove.append(j) for j in re.findall(pattern, i)]

        [orders.pop(i, None) for i in sent + remove]

        return list(orders.keys()), together, sent

    def calc_value(value: int) -> int:
        """
        Высчитывает вес книги в граммах основываясь на её цене
        5800-9000 руб -> 500 г
        9000-11500 руб -> 1 кг
        11500 и больше -> 2 кг

        Args:
            value (int): Цена книги

        Returns:
            int: Вес книги в килограммах
        """
        if value in range(5800, 9000):
            return 500
        elif value in range(9000, 11500):
            return 1000
        elif value >= 11500:
            return 2000
        else:
            logging.critical("Цена заказа не является допустимым числом")
            raise CI("Цена книги указана не верно, не получается получить вес, пропускаем книгу")

    def delivery_parse(delivery_info: str) -> tuple:
        """
        Ищет всю информацию о доставке в отформаттированной строке

        Args:
            delivery_info (str): Строка с информацией о доставке

        Returns:
            tuple:
                delivery_point: Куда доставлять
                tariff_code: Код тарифа у сдэка
                delivery_recipient_cost: Стоимость доставки объявленная покупателю
        """
        delivery_list = delivery_info.replace("\t", "")\
            .replace("\r", "").replace("<br />", "").split(" / ")

        if delivery_list[0] == "Самовывоз":
            raise CI("Самовывоз")

        id_patt = r"\[\w{1,9}\]"
        match = re.search(id_patt, delivery_list[0])
        delivery_point_id = match[0][1:-1] if match else None

        # Посмотреть как упростить оба варианта
        if delivery_point_id is not None:
            delivery_point = delivery_point_id

            recipient_cost = delivery_list[0].split("\n")[1]
            delivery_recipient_cost = 0 if recipient_cost == "Бесплатно" else \
                int(recipient_cost.split(" ")[0])

        else:
            delivery_temp = delivery_list[0].split("\n")
            delivery_temp = [i for i in delivery_temp if i]
            logging.info("Проверка на случай неправильности обработки доставки до двери:")
            logging.info(delivery_temp)
            # Изначально был 1ый элемент листа
            delivery_point = delivery_temp[-2]

            recipient_cost = re.search(r"\d{1,100}", delivery_temp[2])
            delivery_recipient_cost = 0 if not recipient_cost else \
                int(recipient_cost[0].split(" ")[0])

        try:
            tariff_code = CDEK_API.delivery_types[delivery_list[-1].split("\n")[0]]
        except:
            raise CI("Поле доставки не соответствует шаблону")
        return (delivery_point, tariff_code, delivery_recipient_cost)


def main():
    Scriptlets.dir_check()
    books = Scriptlets.operating_mode()

    wcm = WireCRM_API(config.crm_key)
    cdek = CDEK_API(config.cdek_urls, config.id, config.secret)

    not_done_books = list()
    for book in books:
        try:
            print()
            with open(log_filename, "a") as f:
                f.write("\n==============\nКнига №" + str(book) + "\n==============\n")
            crm_info = wcm.get_book_info(book)
            order = asdict(Book(crm_info))
            order.pop("_crm_info")

            json = write_json(order, test=not config.c_type)
            logging.info(json)

            _, uuid = cdek.send_to_cdek(json)
            cdek.get_info(uuid)
            _, ans = cdek.make_receipt(uuid)
            cdek.get_receipt(ans, book)
        except CI as ci:
            print(f"Книга {book}: {ci.msg}")
            logging.error("Пропускаем книгу №" + str(book) + ": " + ci.msg)
            not_done_books.append(book)
            continue
    with open(log_filename, "a") as f:
        f.write("\n============\n" + "Конец работы\n")
    print("\nКниги, которые не были обработаны скриптом:")
    print(*not_done_books)


if __name__ == "__main__":
    log_filename = "tmp/log.log" if config.c_type else "tmp/test.log"
    # Записываем логи в файл cool.log
    log_format = "%(levelname)s\t[%(asctime)s]: %(message)s"
    date_format = "%H:%M:%S"
    logging.basicConfig(filename=log_filename, encoding='utf-8', level=logging.DEBUG,
                        format=log_format, datefmt=date_format)
    # Исключаем логирование всех реквестов, оставляем только ошибки и предупреждения
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    # Вписываем дату в логовский файл
    with open(log_filename, "a") as f:
        dt = Scriptlets.current_datetime()
        f.write(f"""\n\n
[{dt[0]} | {dt[1]}]\n""")

    main()
