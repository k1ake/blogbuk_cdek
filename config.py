# https://api-docs.cdek.ru/29923926.html
# Сдековский апи, всё что надо - там

# https://github.com/WireCRM/api
# ЦРМовский апи, по каждому модулю смотреть:
# https://wirecrm.com/help/api-[module]

# https://dadata.ru/api/clean/address/
# Апи для получения адреса из строки

'''
#===================================================#
        Боевой конфиг: c_type - True
        Тестовый конфиг: c_type - False
#===================================================#
'''
if __name__ != "__main__":
    c_type = False

    crm_key = ""
    ap5Login = ""
    ap5Password = ""

    # Еcли конфиг боевой
    if c_type:
        id = ""
        secret = ""
        cdek_send_url = ""
        cdek_head_url = ""
        cdek_info_url = ""
        cdek_mpdf_url = ""
        cdek_gpdf_url = ""
        cdek_deli_url = ""
    # Тестовый конфиг
    else:
        id = ""
        secret = ""
        cdek_send_url = ""
        cdek_head_url = ""
        cdek_info_url = ""
        cdek_mpdf_url = ""
        cdek_gpdf_url = ""
        cdek_deli_url = ""
        print("Используется тестовый конфиг")

    cdek_urls = {
        "SEND": cdek_send_url,
        "HEAD": cdek_head_url,
        "INFO": cdek_info_url,
        "MPDF": cdek_mpdf_url,
        "GPDF": cdek_gpdf_url,
        "DELI": cdek_deli_url
    }
    # Проверка на наличие значений:
    check = {"Ключ CRM": bool(crm_key),
             "ID СДЭК": bool(id),
             "Секрет СДЭК": bool(secret),
             "Логин ap5": bool(ap5Login),
             "Пароль ap5": bool(ap5Password)}

    if False in check.values():
        for i, v in check.items():
            if not v: print(f"{i} - отсутствует")
        print()
        raise NameError("Не хватает одного из ключей, смотри вывод выше")


else:
    print("Это конфигурационный файл, он не предназначен для запуска")
